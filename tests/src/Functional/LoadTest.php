<?php

namespace Drupal\Tests\bootstrap_pages\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality of Bootstrap Pages.
 *
 * @group bootstrap_pages
 */
class BasicTestCase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Module(s) for core functionality.
    'node',
    'views',

    // This module.
    'bootstrap_pages',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Set the front page to "node".
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);
  }

  /**
   * Make sure the site still works. For now just check the front page.
   */
  public function testTheSiteStillWorks() {
    // Load the front page.
    $this->drupalGet('<front>');

    // Confirm that the site didn't throw a server error or something else.
    $this->assertSession()->statusCodeEquals(200);

    // Confirm that the front page contains the standard text.
    $this->assertSession()->pageTextContains('Welcome to Drupal');
  }

}
